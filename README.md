# BucketList Front End Web Application

1. Link to Front End Web Application - https://gitlab.com/Jonathan.Francis/frontend_bucketlist_vacation
2. Link to Back End Web Application - https://gitlab.com/Jonathan.Francis/backend_nodejs_bucketlist_vacation


# Information about the .env file

1. This project uses the .env file to store important information for this application
2. Rename the .envExample to .env
3. The Backend API will be using http://localhost:5000 as mentioned in our backend application.

# Run this project using docker

1. You can run this project using docker build and docker run
2. Ensure that you have first set up the backend project
3. Install docker if you don't have it already
4. In the Dockerfile, update the REACT_APP_API to http://localhost:5000
5. Use the following commands to run the react app

a) docker build -t frontend .
b) docker run -d -p  3000:3000 frontend (you can change the first port to an available port on your system)

6. Load up localhost:3000 in your browser and it should now show the frontend
7. You can test everything by logging in as a user or as an admin once the MySQL and the Back End are running - see Back End link for details:

Sign in as Test User:
E-mail: peter@parker.com
Password: iamspiderman

Sign in as Test Administrator:
Email: admin@admin.com
Password: adminpassword

# Run this project using npm

1. To run this project you will need to clone this repo using SSH or HTTP
2. Open the folder in VS Code or applicable source-code editor with access to terminal
3. When in the terminal, use npm install to install node dependencies
4. The project uses http://localhost:3000 to run so ensure that port 3000 is not in use
5. In the terminal, type in 'npm start' and go to http://localhost:3000 in your preferred browser (it should open automatically)
6. From here you can Navigate the React App
7. If for some reason your project is not starting up, you may need to install the following dependency - npm install @craco/craco --legacy-peer-deps
8. You can test everything by logging in as a user or as an admin once the MySQL and the Back End are running - see Back End link for details:

Sign in as Test User:
E-mail: peter@parker.com
Password: iamspiderman

Sign in as Test Administrator:
Email: admin@admin.com
Password: adminpassword



# To fully utilize this, make sure to run the node backend that is included in this group and follow the instructions there (Remember to update the port for the backend to 5000)

# Current Issues

1. Contact us is using the database and there is no e-mail functionality attached to it.
2. No ability to upload a new profile picture. It is just a link to an image of your choice (many people use Gravatar so this may not be too bad)
3. Suggested trips isn't functional and would require some work done.
4. Site isn't 100% mobile friendly at this time (mostly the menu bar not scaling down)
5. Various UI/UX issues that need to be addressed.
6. Submit and edit button on admin page need to be updated to show in page instead of behind footer.
7. Add in forget password functionality
