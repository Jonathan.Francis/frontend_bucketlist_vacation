FROM node:lts-slim

WORKDIR /app

COPY package*.json ./

RUN npm install --force

COPY ./ ./

ENV REACT_APP_API=http://localhost:5000

CMD npm run start
