import React from "react";
import { useState, useEffect } from "react";
import Login from "./components/pages/Login";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./components/pages/Home";
import AdminHome from "./components/pages/AdminHome";
import AdminProfile from "./components/pages/AdminProfile"
import AdminCreate from "./components/pages/AdminCreate";
import AdminFind from "./components/pages/AdminFind"
import AdminView from "./components/pages/AdminView";
import AdminContact from "./components/pages/AdminContact";
import AdminUserProfileList from "./components/pages/AdminUserProfileList";
import AdminUserProfileView from "./components/pages/AdminUserProfileView";
import ViewContactSubmission from "./components/pages/AdminViewContactSubmission";
import Register from "./components/pages/Register";
import BucketList from "./components/pages/BucketList";
import Navigation from "./components/shared/Navigation";
import Footer from "./components/shared/Footer";
import Trips from "./components/pages/Trips";
import AddBucket from "./components/pages/AddBucket";
import AddTrip from "./components/pages/AddTrip";
import EditTrip from "./components/pages/EditTrip";
import Featured from "./components/pages/Featured";
import UserProfileFull from "./components/pages/UserProfileFull";
import authVerify from "./components/helpers/authVerify";
import isAuthenticated, { isAdmin } from "./components/helpers/authHelper";
import PrivateRoute from "./components/shared/PrivateRoute";
import AdminRoute from "./components/shared/AdminRoute";
import ContactUs from "./components/pages/ContactUs"

import { CssBaseline } from "@mui/material";

function App() {

  const [auth, setAuth] = useState(false);
  const [admin, setAdmin] = useState(false);

  useEffect(() => {
    if (isAdmin()) {
      setAdmin(true)
    } else {
      setAdmin(false)
    }
    if (isAuthenticated() && authVerify()) {
      setAuth(true)
    } else {
      setAuth(false)
    }
  }, []);

  return (
    <BrowserRouter>
      <CssBaseline />
      <Navigation auth={auth} setAuth={setAuth} admin={admin} setAdmin={setAdmin}/>
      <Switch>
        <Route exact path="/" component={() => <Login auth={auth} setAuth={setAuth} admin={admin} setAdmin={setAdmin}/>} />
        <Route exact path={"/Register"} component={Register} />
        <PrivateRoute exact path={"/Home"} component={Home} />
        <AdminRoute exact path={"/AdminHome"} component={AdminHome} />
        <PrivateRoute exact path={"/Admin/Create"} component={AdminCreate} />
        <AdminRoute exact path={"/Admin/Profile"} component={AdminProfile} />
        <AdminRoute exact path={"/Admin/Contact"} component={AdminContact} />
        <AdminRoute exact path={"/Admin/Contact/View/:id"} component={ViewContactSubmission} />
        <AdminRoute exact path={"/Admin/User/Profile/List"} component={AdminUserProfileList} />
        <AdminRoute exact path={"/Admin/User/Profile/List/:id"} component={AdminUserProfileView} />
        <AdminRoute exact path={"/Admin/View"} component={AdminView} />
        <AdminRoute exact path={"/Admin/Find"} component={AdminFind} />
        <PrivateRoute exact path={"/UserProfile"} component={UserProfileFull} />
        <PrivateRoute exact path={"/ContactUs"} component={ContactUs} />
        <PrivateRoute exact path={"/Trips"} component={Trips} />
        <PrivateRoute exact path={"/AddTrip"} component={AddTrip} />
        <PrivateRoute exact path={"/Featured"} component={Featured} />
        <PrivateRoute exact path={"/EditTrip/:tripid"} component={EditTrip} />
        <PrivateRoute exact path={"/AddBucket"} component={AddBucket} />
        <PrivateRoute exact path={"/BucketList"} component={BucketList} />
      </Switch>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
