export const parseJwt = (token: string | "") => {
    try {
        return JSON.parse(atob(token.split(".")[1]));
    } catch (e) {
        return false;
    }
};

export const isAdmin = () => {
    let admin = parseJwt(sessionStorage.getItem("token") || "{}")
    // eslint-disable-next-line
    if (admin.admin == 1) {
        return true
    } else {
        return false
    }
}

const isAuthenticated = () => {
    try {
        return parseJwt(sessionStorage.getItem("token") || "{}");
    } catch (error) {
        console.error(error);
        return false;
    }
};

export default isAuthenticated;
