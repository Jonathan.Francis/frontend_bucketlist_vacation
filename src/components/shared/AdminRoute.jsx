import React from "react";
import { Route, Redirect } from "react-router-dom";
import {isAdmin} from "../helpers/authHelper";


const AdminRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      component={(props) =>
        isAdmin() ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );

export default AdminRoute;