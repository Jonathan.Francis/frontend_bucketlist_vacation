import * as React from 'react';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/material/MenuItem';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';

var style = {
    backgroundColor: "#1f2937",
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "80px",
    width: "100%",
}


export default function Footer() {
    return (
            <div style={style}>
                <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        <img src="/logo.png" height="50" alt="Bucket List Vacation Logo" />
                    </Typography>
                    <a href="https://www.facebook.com/Bucket-List-Vacations-109408048055153" target="_blank" rel="noreferrer"><FacebookIcon color="primary" sx={{ fontSize: 30 }} /></a>
                    <a href="https://www.instagram.com/bucketlistvactn" target="_blank" rel="noreferrer"><InstagramIcon color="primary" sx={{ fontSize: 30 }} /></a>
                </Toolbar>
            </div>
    );
}
