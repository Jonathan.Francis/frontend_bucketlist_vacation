import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/material/MenuItem';
import { useHistory } from "react-router-dom";

export default function Navigation(props) {
  let history = useHistory();

  // Logout function to remove token and redirect to login page
  const logout = (event) => {
    event.preventDefault();
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("userid");
    sessionStorage.removeItem("admin");
    props.setAuth(false);
    history.push("/");
  };

  if (props.auth && props.admin) {
    return (
      <Box sx={{ flexGrow: 1 }}>
        <AppBar sx={{ bgcolor: "#1f2937" }} position="static">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <a href="/AdminHome"><img src="/logo.png" height="50" alt="Bucket List Vacation Logo" /></a>
            </Typography>
            <Button onClick={() => history.push('/AdminHome')} color="inherit">Home</Button>
            <Button onClick={logout} color="inherit">Logout</Button>
          </Toolbar>
        </AppBar>
      </Box >
    )
  } else if (props.auth) {
    return (
      <Box sx={{ flexGrow: 1 }}>
        <AppBar sx={{ bgcolor: "#1f2937" }} position="static">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <a href="/Home"><img src="/logo.png" height="50" alt="Bucket List Vacation Logo" /></a>
            </Typography>
            <Button onClick={() => history.push('/Home')} color="inherit">Home</Button>
            <Button onClick={() => history.push('/Featured')} color="inherit">Featured</Button>
            <Button onClick={() => history.push('/Trips')} color="inherit">Trips</Button>
            <Button onClick={() => history.push('/ContactUs')} color="inherit">Contact Us</Button>
            <Button onClick={() => history.push('/Admin/Create')} color="inherit">Administrator</Button>
            <Button onClick={logout} color="inherit">Logout</Button>
          </Toolbar>
        </AppBar>
      </Box >
    )
  } else {
    return (
      <Box sx={{ flexGrow: 1 }}>
        <AppBar sx={{ bgcolor: "#1f2937" }} position="static">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <a href="/"><img src="/logo.png" height="50" alt="Bucket List Vacation Logo" /></a>
            </Typography>
          </Toolbar>
        </AppBar>
      </Box >
    )
  }
}
