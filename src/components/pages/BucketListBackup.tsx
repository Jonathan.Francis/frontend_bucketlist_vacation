import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { Container, CssBaseline } from '@mui/material'
import { Button, CardActionArea, CardActions, Stack } from '@mui/material';
import React from "react";
import { useHistory } from 'react-router';


export default function BucketList() {
  const history = useHistory();

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };
  
  const card = (
    <React.Fragment>
      <Card sx={{ width: "100%" }} variant="outlined">
        <CardActionArea>
          <CardMedia>
            <img style={{ width: "100%" }} src="https://source.unsplash.com/random" alt=''></img>
          </CardMedia>
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              Bucket Item
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button onClick={() => history.push("/EditTrip")} variant="contained">
            View/Edit
          </Button>
          <Button size="small" color="primary">
            Share
          </Button>
        </CardActions>
      </Card>
    </React.Fragment>
  );
  
  return (
    <Container component="main">
      <CssBaseline />
      <Typography
        component="h1"
        variant="h5"
        align="center"
        color="text.primary"
        gutterBottom
        sx={{
          marginTop: 4
        }}
      >
        Bucket List Items
      </Typography>
      <Typography variant="subtitle1" align="center" color="text.secondary" paragraph>
        Manage your bucket list items here.
      </Typography>
      <Stack
        direction="row"
        spacing={4}
        justifyContent="center"
      >
        <Button onClick={() => history.push("/AddTrip")} variant="contained">Add Bucket Item</Button>
      </Stack>
      <Carousel responsive={responsive}>
        <div>{card}</div>
        <div>{card}</div>
        <div>{card}</div>
        <div>{card}</div>
        <div>{card}</div>
        <div>{card}</div>
      </Carousel>
    </Container>
  );
}
