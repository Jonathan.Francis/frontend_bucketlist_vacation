import React, { useState, useEffect } from "react";
import { makeStyles } from '@mui/styles';
import { CssBaseline, TextField, Button, Grid, Container } from '@mui/material';
import Alert from '@material-ui/lab/Alert';

//CSS
const useStyles = makeStyles({
  table: {
    width: '100%',
    marginBottom: '50px',
  },
  grid: {
    marginBottom: '50px',
    width: '100%',
    alignItems: "center"
  },
  gridHead: {
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 15,
    background: 'aliceblue',
    fontWeight: 'bold',
    width: '30%',
    minWidth: 150,
  },
  gridData: {
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 15,
    width: '70%',
    minWidth: 200,
  },
  margin: {
    marginBottom: 10
  }
});


//Page
const AdminProfile = ({ adminData }) => {

  const classes = useStyles();

  const [admin, setAdmin] = useState({});
  const [updateAdmin, setUpdateAdmin] = useState({})
  const [editMode, setEditMode] = useState(false)
  const [status, setStatus] = useState(false);
  const [errorMessage, setErrorMessage] = useState("")
  let userid = sessionStorage.getItem("userid").replace(/"/g, "");



  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(process.env.REACT_APP_API + `/admin/get/${userid}`,
        {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
          }
        });

      res.json().then((res) => setAdmin(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [userid]);

  const toggleEdit = () => {
    setUpdateAdmin(admin)
    setEditMode(true)
  }

  const cancel = () => {
    setStatus(false)
    setErrorMessage("")
    setEditMode(false)
    setUpdateAdmin(admin)
  }

  const handleChange = (e) => {
    let value = e.target.value
    let key = e.target.name
    setStatus(false)
    setUpdateAdmin((prevData) => ({ ...prevData, [key]: value }))
  }

  const formSubmit = async event => {
    event.preventDefault()
    const response = await fetch(process.env.REACT_APP_API + `/admin/update/${userid}`,
      {
        method: 'PATCH',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
        body: JSON.stringify(updateAdmin)
      })

    // const payload = await response.json()
    if (response.status === 200) {
      setEditMode(false)
      setStatus("success")
      setErrorMessage("Profile updated!")
    } else {
      setStatus("error")
      setErrorMessage("Profile could not be updated.")
    }
  }

  return (
    <Container>
      <CssBaseline />
      <h1 className={classes.margin}>Hello, {admin.fullName}!</h1>
      {!status || <Alert className={classes.margin} severity={status || "info"}>{errorMessage}</Alert>}
      <form className={classes.margin} noValidate autoComplete="off" onSubmit={(e) => formSubmit(e)}>
        <Grid container spacing={0} className={classes.grid}>
          <Grid item className={classes.gridHead}>Full Name</Grid>
          {editMode ? <TextField fullWidth className={classes.gridData} id="addFullName" value={updateAdmin.fullName} name="fullName" label="" onChange={(e) => handleChange(e)} />
            : <Grid item className={classes.gridData}>{admin.fullName}</Grid>}
          <Grid item className={classes.gridHead}>Username</Grid>
          {editMode ? <TextField fullWidth className={classes.gridData} id="addUsername" value={updateAdmin.username} name="username" onChange={(e) => handleChange(e)} />
            : <Grid item className={classes.gridData}>{admin.username}</Grid>}
          <Grid item className={classes.gridHead}>Email</Grid>
          {editMode ? <TextField fullWidth className={classes.gridData} id="addEmail" value={updateAdmin.email} name="email" onChange={(e) => handleChange(e)} />
            : <Grid item className={classes.gridData}>{admin.email}</Grid>}
          <Grid item className={classes.gridHead}>Phone Number</Grid>
          {editMode ? <TextField fullWidth className={classes.gridData} id="addPhoneNumber" value={updateAdmin.phoneNumber} name="phoneNumber" onChange={(e) => handleChange(e)} />
            : <Grid item className={classes.gridData}>{admin.phoneNumber}</Grid>}

          <Grid item className={classes.gridHead}>Admin</Grid>
          <Grid item className={classes.gridData}>{admin.admin ? "Yes" : "No"}</Grid>

        </Grid>
        <Button className={classes.margin} variant="contained" onClick={() => toggleEdit()}>EDIT PROFILE</Button>
        <Button className={classes.margin} variant="contained" color="error" onClick={() => cancel()}>CANCEL</Button>
        <Button type="submit" />
      </form>
      <br></br>
      <br></br>
      <br></br>
    </Container>
  );
};

export default AdminProfile;
