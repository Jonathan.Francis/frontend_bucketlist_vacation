import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import ContactOutlinedPageIcon from '@mui/icons-material/ContactPage';
import parseJwt from '../helpers/authHelper'
import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';

export default function ContactUs() {
  const history = useHistory()
  const token = sessionStorage.getItem("token");
  const email = parseJwt(token).email;
  const [user, setUser] = useState([]);
  const [subject, setSubject] = useState("")
  const [message, setMessage] = useState("")


  const fetchData = async () => {
    const res = await fetch(process.env.REACT_APP_API + `/auth/user/${email}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });
    res
      .json()
      .then((res) => setUser(res))
      .catch((err) => console.log(err));
  }

  // eslint-disable-next-line
  useEffect(() => { fetchData() }, []);

    // This function submits the contact form to the backend and returns either an error on a field being incorrect or succcess if form is accepted.
    const formSubmit = async (event) => {
      event.preventDefault();
      const response = await fetch(process.env.REACT_APP_API + `/contact/entries`, {
        method: "POST",
        headers: {
          'Accept': "application/json",
          'Content-Type': "application/json",
          Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({userid: user.userid, email: email, subject: subject, message: message}),
      });
      if (response.status >= 400) {
        console.error()
      } else {
        history.push('/Home')
      }
    };
  

  return (
    <Container component="main" maxWidth="sm">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          marginBottom: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar>
          <ContactOutlinedPageIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Contact Us
        </Typography>
        <Typography variant="subtitle1">
          Please complete the form below.
        </Typography>
        <form onSubmit={formSubmit}>
        <TextField
          helperText="Your E-mail Address"
          margin="normal"
          id="email"
          maxRows={4}
          value={user.email}
        />
          <TextField
          fullWidth
          helperText="Subject"
          margin="normal"
          id="email"
          maxRows={4}
          value={subject}
          onChange={(e) => setSubject(e.target.value)}
        />
        <TextField
          helperText="Your Message"
          margin="normal"
          fullWidth
          id="message"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          multiline
          rows={4}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          Submit
        </Button>
        </form>
        <br></br>
        <br></br>
        <br></br>
      </Box>
    </Container>
  );
}
