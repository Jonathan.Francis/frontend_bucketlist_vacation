import { useEffect, useState } from "react";
import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';import { useHistory } from "react-router-dom";
import { makeStyles } from '@mui/styles';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';

const useStyles = makeStyles({
  table: {
    width: '100%'
  },
});

const AdminContact = () => {
  const token = sessionStorage.getItem("token");
  const [entries, setEntries] = useState([]);
  const history = useHistory();
  const classes = useStyles();


  // useEffect hook is used here to fetch the contact form entries and display it on the page
  // Passing in the second argument of token ensures that the useEffect hook is not run infinitely and will only do a check once to see if the token value is changed and display the entries
  const getData = async () => {
    const response = await fetch(
      process.env.REACT_APP_API + `/contact/entries`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const data = await response.json();
    setEntries(data);
  };
  
  // eslint-disable-next-line
  useEffect(() => {getData();}, [token]);

  const viewSubmission = (event, entries) => {
    event.preventDefault();
    let path = `/Admin/Contact/View/${entries.contactid}`
    history.push(path, entries)
  }

  const deleteSubmission = async (event, entries) => {
    event.preventDefault();
    await fetch(process.env.REACT_APP_API + `/contact/entries/delete/${entries.contactid}`, {
      'method': 'DELETE',
      'headers': {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    getData()
  }

  let sortedList = [...entries]
  sortedList.sort((a, b) => {
    if (a.contactid > b.contactid) {
      return -1
    }
    if (a.contactid < b.contactid) {
      return 1
    }
    return 0;
  })

  return (
  
   <Container component="main">
    <h1 align="center">All Submitted Messages</h1>
    <Box sx={{
        marginTop: 4,
        marginBottom:48,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: "center"
      }}>
    <TableContainer className={classes.table} component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Contact ID</TableCell>
            <TableCell>User ID</TableCell>
            <TableCell>Subject</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Message</TableCell>
            <TableCell></TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {sortedList.map((entry) => (
            <TableRow key={entry.contactid}>
              <TableCell>{entry.contactid}</TableCell>
              <TableCell>{entry.userid}</TableCell>
              <TableCell>{entry.subject}</TableCell>
              <TableCell>{entry.email}</TableCell>
              <TableCell>{entry.message}</TableCell>
              <TableCell><Button color="primary" variant="contained" onClick={(e) => viewSubmission(e, entry)} component={Paper}>View Message</Button></TableCell>
              <TableCell><Button variant="contained" color="error" onClick={(e) => deleteSubmission(e, entry)} component={Paper}>Delete Message</Button></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </Box>
    </Container>
  );
};

export default AdminContact;
