import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useHistory } from 'react-router';
import { useState } from 'react';
import { Alert } from '@mui/material';

export default function Login(props: any) {
  let history = useHistory();
  
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginFailure, setLoginFailure] = useState(false);

  const loginSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/auth`, {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    });
    const payload = await response.json();
    if (response.status >= 400) {
      console.error();
      setLoginFailure(true)
    } else {
      props.setAuth(true)
      sessionStorage.setItem("token", payload);
      const response2 = await fetch(process.env.REACT_APP_API + `/auth/user/${email}`, {
        method: "GET",
        headers: {
          'Authorization': `Bearer ${payload}`
        }
      })
      const currentUser = await response2.json()
      delete currentUser.password
      sessionStorage.setItem('userid', JSON.stringify(currentUser.userid))
      sessionStorage.setItem('admin', JSON.stringify(currentUser.admin))
      if (currentUser.admin === 1) {
        let adminpath = "/AdminHome"
        props.setAdmin(true)
        history.push(adminpath)
      } else {
        let userpath = "/Home"
        props.setAdmin(false)
        history.push(userpath)
      }      
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          marginBottom: 37,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Typography variant="subtitle1">
          Please fill in your credentials to sign in.
        </Typography>
        {loginFailure && (<Alert severity="error">Invalid credentials. Please try again.</Alert>)}
        <Box component="form" onSubmit={loginSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            variant="standard"
            autoFocus
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            variant="standard"
            autoComplete="current-password"
            onChange={(e) => setPassword(e.target.value)}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="/Register" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}
