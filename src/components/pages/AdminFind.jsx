import React, { useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import { TextField, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';
import DeletePopup from "../shared/DeletePopup.jsx"
import Box from '@mui/material/Box';

//CSS
const useStyles = makeStyles({
  table: {
    width: '50%',
    marginBottom: '50px',
  },
  grid: {
    marginBottom: '50px',
    width: '50%',
    marginLeft: 'auto',
    marginRight: 'auto', 
    alignItems: 'center',
  },
  gridHead: {
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 15,
    background: 'aliceblue',
    fontWeight: 'bold',
    width: '30%',
    minWidth: 150,
  },
  gridData: {
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 15,
    width: '70%',
    minWidth: 200,
  },
  plain: {
    textDecoration: 'none'
  },
  textfield: {
    marginBottom: '50px',
    width: '50%',
    marginLeft: 'auto',
    marginRight: 'auto', 
    alignItems: 'center',
  }


});

const AdminFind = ({ setAdminid }) => {
  const classes = useStyles();

    const [savedRes, setSavedRes] = useState("")
    const [admins, setAdmins] = useState([]);
    const [filteredAdmins, setFilteredAdmins] = useState([]);
    const [formData, updateFormData] = useState({});
    const [anchorEl, setAnchorEl] = useState(null);
    const [disabled, setDisabled] = useState(false)

    const handlePop = (event) => {
      setAnchorEl(anchorEl ? null : event.currentTarget);
      setDisabled(true)
    };

    const handleClick = (id) => {
      setAdminid(id)
    }

    const handleChange = (e) => {
    let value = e.target.value
    let key = e.target.name
    updateFormData({ ...formData, [key]:value })
  }

    const formSubmit = async event => {
      event.preventDefault()
      const response = await fetch (process.env.REACT_APP_API + `/admin/get/search`, 
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type' : 'application/json'
        },
        body: JSON.stringify(formData)
      })

      const payload = await response.json()
      if (response.status === 200) {
        setFilteredAdmins(payload)
        setSavedRes(response)
      } else {
        console.log("error")
      }
    }



    const handleDelete = async (id) => {
      const res = await fetch(process.env.REACT_APP_API + `/admin/delete/${id}`,
          {
            method: 'DELETE',
            headers: {
              'Accept': 'application/json',
              'Content-type' : 'application/json'
          }
        });

        res.json().then((res)=>setSavedRes(res))
        .catch((err)=>console.log(err));
      }

  useEffect(()=>{
    const fetchData = async () => {
      const res = await fetch(process.env.REACT_APP_API + `/admin/get/all`,
          {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-type' : 'application/json'
          }
        });

        res.json().then((res)=>setAdmins(res))
        .catch((err)=>console.log(err));
      }
        fetchData();
    }, [savedRes]);

    const getAdmins = () => {
      if (filteredAdmins && filteredAdmins.length > 0) {
        return filteredAdmins.map((admin) => (
            <TableRow>
              <TableCell>{admin.fullName}</TableCell>
              <TableCell>{admin.username}</TableCell>
              <TableCell>{admin.email}</TableCell>
              <TableCell><Link to="/Admin/View" variant="contained" className={classes.plain} onClick={() => handleClick(admin.userid)}><Button value={admin.userid} variant="contained" onClick={(e) => handleClick(e)}>View & Edit</Button></Link></TableCell>
              <TableCell><Button disabled={disabled} color="secondary" variant="contained" onClick={handlePop}>DELETE</Button></TableCell>
              <DeletePopup 
                anchorEl={anchorEl}
                setAnchorEl={setAnchorEl}
                handleDelete={handleDelete}
                adminid={admin.userid}
                setDisabled={setDisabled}
              />
            </TableRow>
          ))
      } else {
        return admins.map((admin) => (
            <TableRow>
              <TableCell>{admin.fullName}</TableCell>
              <TableCell>{admin.username}</TableCell>
              <TableCell>{admin.email}</TableCell>
              <TableCell><Link to="./accounts/details" variant="contained" className={classes.plain} onClick={() => handleClick(admin.userid)}><Button value={admin.userid} variant="contained" onClick={(e) => handleClick(e)}>View & Edit</Button></Link></TableCell>
              <TableCell><Button disabled={disabled} color="secondary" variant="contained" onClick={handlePop}>DELETE</Button></TableCell>
              <DeletePopup 
                anchorEl={anchorEl}
                setAnchorEl={setAnchorEl}
                handleDelete={handleDelete}
                adminid={admin.userid}
                setDisabled={setDisabled}
              />
            </TableRow>
          ))
      }
    }

  return (
      <Box
          sx={{
            marginTop: 8,
            marginBottom: 8,
            marginLeft: 'auto',
            marginRight: 'auto',  
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
      <h3>Search Administrators</h3>
      <br></br>
      <br></br>
       <form noValidate autoComplete="off" onSubmit={(e) => formSubmit(e)}>
        <TextField fullWidth className={classes.textfield} id="AdminFullName" name="fullName" label="Full Name" variant="outlined" onChange={(e)=> handleChange(e)}/>
        <TextField fullWidth className={classes.textfield} id="AdminEmail" name="email" label="E-mail" variant="outlined" onChange={(e)=> handleChange(e)}/>
        <Button
          color="primary"
          type="submit"
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
          text
            >
             Search
            </Button>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
      </form>
      <h3>All Adminstrators</h3>
      <TableContainer className={classes.table} component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Full Name</TableCell>
            <TableCell>Username</TableCell>
            <TableCell>Email</TableCell>
            <TableCell colSpan={2} align="center">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {getAdmins()}
        </TableBody>
      </Table>
    </TableContainer>
    </Box>
  )
}

export default AdminFind