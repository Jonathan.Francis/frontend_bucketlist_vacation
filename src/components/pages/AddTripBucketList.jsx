import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button'
import CheckIcon from '@mui/icons-material/Check';
import EditIcon from '@mui/icons-material/Edit';
import ShareIcon from '@mui/icons-material/Share';
import DeleteIcon from '@mui/icons-material/Delete';
import Container from '@mui/material/Container'
import AddBucket from './AddBucket'

let AddTripBucketList = ({tripid}) => {

  const [savedRes, setSavedRes] = useState("")
  const [list, setList] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(process.env.REACT_APP_API + `/trip/addbucket/${tripid}`, {
        method: "GET",
      });
      res
        .json()
        .then((res) => setList(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [savedRes, tripid]);

  return (
      <Container>
        <AddBucket tripid = {tripid} setSavedRes = {setSavedRes}/>
        <TableContainer component={Paper} sx={{ marginBottom: 8 }}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Title</TableCell>
                <TableCell align="right">Location</TableCell>
                <TableCell align="right">Budget</TableCell>
                <TableCell align="right">Goal</TableCell>
                <TableCell align="right">Due Date</TableCell>
                <TableCell align="right">Done</TableCell>
                <TableCell align="right">Edit</TableCell>
                <TableCell align="right">Share</TableCell>
                <TableCell align="right">Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {list.map((list) => (
                <TableRow
                  key={list.name}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {list.name}
                  </TableCell>
                  <TableCell align="right">{list.location}</TableCell>
                  <TableCell align="right">{list.budget}</TableCell>
                  <TableCell align="right">{list.category}</TableCell>
                  <TableCell align="right">{list.targetDate}</TableCell>
                  <TableCell align="right"><Button size="small" variant="contained" color="success"><CheckIcon /></Button></TableCell>
                  <TableCell align="right"><Button size="small" variant="contained"><EditIcon /></Button></TableCell>
                  <TableCell align="right"><Button size="small" variant="contained"><ShareIcon /></Button></TableCell>
                  <TableCell align="right"><Button size="small" variant="contained" color="error"><DeleteIcon /></Button></TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
  );}

export default AddTripBucketList