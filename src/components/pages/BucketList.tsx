import AddIcon from '@mui/icons-material/Add';
import { Typography, Button, Box, Grid } from '@mui/material';
import * as React from 'react';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';


export default function BucketList() {
  return (
    <Box sx={{ flexGrow: 1, marginTop: 4, marginBottom: 4, p: 1, alignItems: "center" }}>
      <Typography component="h1" variant="h5">
        My Bucketlist Goals and Suggested Goals
      </Typography>
      <Grid container spacing={3} sx={{ marginTop: 1 }}>
        <Grid item xs={3}>
          <ImageListItem>
            <img
              src='https://image.freepik.com/free-photo/colosseum-rome-morning-sun-italy_119101-11.jpg'
              alt='Italy Colosseum'
            />
            <ImageListItemBar
              title='Rome, Colosseum'
              subtitle='Italy'
              actionIcon={
                <Button sx={{ color: 'rgba(255, 255, 255, 0.54)' }}>
                  <AddIcon />
                </Button>
              }
            />
          </ImageListItem>
        </Grid>
        <Grid item xs={3}>
          <ImageListItem>
            <img
              src='https://images.ctfassets.net/cnu0m8re1exe/1tI0J6fFR4TQAU7YApSjcE/3aedcf058c5f2cd9212b86731541ac3d/shutterstock_745306897.jpg'
              alt='Italy Pisa'
            />
            <ImageListItemBar
              title='Pisa, Italy'
              subtitle='Italy'
              actionIcon={
                <Button sx={{ color: 'rgba(255, 255, 255, 0.54)' }}>
                  <AddIcon />
                </Button>
              }
            />
          </ImageListItem>
        </Grid>
        <Grid item xs={3}>
          <ImageListItem>
            <img
              src='https://lp-cms-production.imgix.net/2021-03/shutterstock_304631102.jpg?auto=format&fit=crop&sharp=10&vib=20&ixlib=react-8.6.4&w=850'
              alt='Louvre'
            />
            <ImageListItemBar
              title='Louvre, France'
              subtitle='France'
              actionIcon={
                <Button sx={{ color: 'rgba(255, 255, 255, 0.54)' }}>
                  <AddIcon />
                </Button>
              }
            />
          </ImageListItem>
        </Grid>
        <Grid item xs={3}>
          <ImageListItem>
            <img
              src='https://www.kansascity.com/latest-news/l7bhyq/picture236725428/alternates/FREE_1140/KCM_ChiefsPackers_1784_102719.JPG'
              alt='Breakfast'
            />
            <ImageListItemBar
              title='Green Bay Packers'
              subtitle='NFL'
              actionIcon={
                <Button sx={{ color: 'rgba(255, 255, 255, 0.54)' }}>
                  <AddIcon />
                </Button>
              }
            />
          </ImageListItem>
        </Grid>
      </Grid>
    </Box>
  );
}