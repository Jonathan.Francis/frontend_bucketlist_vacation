import React, { useState, useEffect } from "react";
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useHistory, Link } from "react-router-dom";

export default function Trips() {
    const history = useHistory();

    function addTripRoute() {
      let path = `/AddTrip`
      history.push(path);
    };

    let userid = sessionStorage.getItem("userid").replace(/"/g, "");
    const [list, setList] = useState([]);
    const [savedRes, setSavedRes] = useState("")
  
    useEffect(() => {
      async function fetchData() {
        const res = await fetch(process.env.REACT_APP_API + `/trip`, {
            method: "GET"
        });
        res
        .json()
        .then((res) => setList(res))
        .catch((err) => console.log(err));
      }
      fetchData();
    }, [savedRes, userid]);

    const deleteTripItem = async (event, item) => {
      event.preventDefault();
      await fetch(process.env.REACT_APP_API + `/trip/${item.tripid}`, {
        method: "DELETE",
      }).then((response) => setSavedRes(response));
    }
  
  return (
    <Container component="main">
      <CssBaseline />
      <main>
        <Box
          sx={{
            pt: 8,
            pb: 6,
          }}
        >
          <Container>
            <Typography
              component="h1"
              variant="h5"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Take control of your dream vacation
            </Typography>
            <Typography variant="subtitle1" align="center" color="text.secondary" paragraph>
              Take a look at our booking packages. Our NFL Trip Packages include NFL Tickets, Luxury Accommodations at the Marriott, Tailgating, Private Jet to your Destination City, Exclusive Events, Team Jersey, and Face Painting!
            </Typography>
            <Button variant="contained" onClick={addTripRoute}>Add Trip</Button>
          </Container>
        </Box>
        <Container>
        <div>
          <Grid container spacing={4}>
            {list.map((list) => (
              <Grid item key={list} xs={12} sm={6} md={4}>
                <Card
                  sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                >
                  <CardMedia
                    component="img"
                    sx={{
                      // 16:9
                      pt: '0%',
                    }}
                    image="https://source.unsplash.com/random"
                    alt="random"
                  />
                  <CardContent sx={{ flexGrow: 1 }}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {list.name}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button>
                    <Link to={{
                        pathname: `/EditTrip/${list.tripid}`,
                        state: { tripinfo: list}}}>
                        View/Edit
                    </Link>
                    </Button>
                    <Button 
                      variant="contained"
                      color="error"
                      onClick={(e) => deleteTripItem(e, list)}>
                      Delete
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
            <br></br>
            <br></br>
            <br></br>
          </div>
        </Container>
      </main>
    </Container>
  );
}