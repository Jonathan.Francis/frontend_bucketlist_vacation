import * as React from 'react';
import { useState } from 'react'
import { Container, ImageList, ImageListItem, Typography, Button, TextField, Box } from '@mui/material';
import { useHistory } from "react-router-dom";

export default function UserProfileFull(props) {
  const history = useHistory()
  let editProfile = props.location.state
  const token = sessionStorage.getItem("token");
  let userid = sessionStorage.getItem("userid").replace(/"/g, "");
  const [user, setUser] = useState(editProfile);

  const handleSubmit = async (event) => {
    event.preventDefault()
    const res = await fetch(process.env.REACT_APP_API + `/profile/${userid}`, {
      method: "PATCH",
      headers: {
        'Accept': "application/json",
        'Content-Type': "application/json",
        'Authorization': `Bearer ${token}`,
      },
      body: JSON.stringify(user)
    })
    let payload = res.json()
    if (payload.status >= 400) {
      console.error()
    } else {
      history.push('/Home')
    }
  }

  const handleChange = (event) => {
    event.persist();
    setUser((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };


  return (
    <Container component="main">
      <Box sx={{
        marginTop: 4,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: "center"
      }}>

        <Typography component="h1" variant="h5">
          Profile Page
        </Typography>
        <Typography variant="subtitle1">
          You can update your details on this page.
        </Typography>
      </Box>
      <Box sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center"
      }}>
        <ImageList sx={{ width: 300, p: 1 }} cols={1}>
          <ImageListItem>
            <img src={user.image} alt="This is your profile"></img>
          </ImageListItem>
        </ImageList>
        <Typography sx={{ width: 350, p: 1 }}>
          <TextField
            fullWidth
            helperText="Full Name"
            id="fullName"
            name="fullName"
            variant="standard"
            defaultValue={user.fullName}
            type="text"
            onChange={handleChange}
          />
          <TextField
            fullWidth
            helperText="E-Mail"
            id="email"
            name="email"
            variant="standard"
            defaultValue={user.email}
            type="email"
            onChange={handleChange}
          />
          <TextField
            fullWidth
            helperText="Phone Number"
            id="phoneNumber"
            name="phoneNumber"
            variant="standard"
            defaultValue={user.phoneNumber}
            type="tel"
            onChange={handleChange}
          />
          <TextField
            fullWidth
            helperText="Location"
            id="location"
            name="location"
            variant="standard"
            defaultValue={user.location}
            type="text"
            onChange={handleChange}
          />
          <TextField
            fullWidth
            helperText="Interests"
            id="interests"
            name="interests"
            variant="standard"
            defaultValue={user.interests}
            type="text"
            onChange={handleChange}
          />
          <TextField
            fullWidth
            helperText="Username"
            id="username"
            name="username"
            variant="standard"
            defaultValue={user.username}
            type="text"
            onChange={handleChange}
          />
          <TextField
            fullWidth
            helperText="Password"
            id="password"
            name="password"
            variant="standard"
            defaultValue={user.password}
            type="password"
            onChange={handleChange}
            autocomplete="off"
          />
          <TextField
            fullWidth
            helperText="Image"
            id="image"
            name="image"
            variant="standard"
            defaultValue={user.image}
            type="text"
            onChange={handleChange}
          />
        </Typography>
      </Box>
      <Box sx={{ display: 'flex', justifyContent: "center", p: 1 }}>
        <Button size="medium" variant="contained" onClick={handleSubmit}>Update My Profile</Button>
      </Box>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
    </Container>
  )
}
