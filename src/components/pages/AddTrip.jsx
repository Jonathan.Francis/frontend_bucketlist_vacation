import React, { useState } from "react";
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Switch from '@mui/material/Switch';
import { Container, CssBaseline } from '@mui/material'
import { useHistory } from "react-router-dom";
import { v4 as uuidv4 } from 'uuid';
import AddTripBucketList from './AddTripBucketList';

let AddTrip = () => {

  const history = useHistory();
  const tripRoute = () => {
    let path = `/Trips`
    history.push(path);
  };

  const [trip, setTrip] = useState({ tripid: uuidv4(), name: "", description: "", featured: "" });
  const [featured, setFeatured] = useState("");
  trip.featured = featured;
  const [hideBtn, setHideBtn] = useState(false);
  const [hideList, setHideList] = useState(true);
  const [disable, setDisable] = useState(false);

  const handleChange = (event) => {
    setTrip({ ...trip, [event.target.name]: event.target.value });
  };

  const handleFeaturedChange = (event) => {
    setFeatured(event.target.checked);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/trip/addtrip`, {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(trip),
    });
    const payload = await response.json();
    if (response.status >= 400 && payload.message === 'Error: Did not create trip') {
      console.error();
    } else {
      alert("Trip Added")
      setHideBtn(true)
      setHideList(false)
      setDisable(true)
    }
  };

  return (
    <Container component="main">
      <CssBaseline />
      <Typography
        variant="h5"
        align="center"
        gutterBottom
        sx={{
          marginTop: 8
        }}
      >
        Create Trip
      </Typography>
      <form>
        <Stack
          style={{
            maxWidth: "500px",
            margin: "auto"
          }}>
          <TextField
            sx={{ m: 0.5 }}
            disabled={disable}
            name="name"
            label="Trip Name"
            variant="standard"
            value={trip.name}
            required
            onChange={handleChange}>
          </TextField>
          <TextField
            sx={{ m: 0.5 }}
            disabled={disable}
            name="description"
            label="Trip Description"
            variant="outlined"
            value={trip.description}
            multiline
            rows={6}
            onChange={handleChange}>
          </TextField>
          <div>
            Featured
            <Switch
              disabled={disable}
              label="featured"
              name="featured"
              value={trip.featured}
              onChange={handleFeaturedChange} />
          </div>
          {!hideBtn &&
            <div>
              <Button
                variant="contained"
                type="submit"
                onClick={handleSubmit}>
                Create
              </Button>
              <Button
                variant="text"
                onClick={tripRoute}>
                Cancel
              </Button>
            </div>
          }
        </Stack>
      </form>
      {!hideList &&
        <AddTripBucketList tripid={trip.tripid} />
      }
      {!hideList &&
        <Button
          variant="contained"
          onClick={tripRoute}>
          Done
        </Button>
      }
    </Container>
  );
}

export default AddTrip