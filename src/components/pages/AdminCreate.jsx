import React, { useState } from 'react';
import Container from '@mui/material/Container';
import { FormControlLabel, TextField, Grid, Radio, RadioGroup, FormLabel, Button, Box } from '@mui/material';
import { Alert } from '@mui/material';

const AdminCreate = () => {
  const [formData, updateFormData] = useState({});
  const [status, setStatus] = useState(false);
  const [errorMessage, setErrorMessage] = useState("")

  const handleChange = (e) => {
    let value = e.target.value
    let key = e.target.name
    setStatus(false)
    updateFormData({ ...formData, [key]: value })
  }

  const formSubmit = async event => {
    event.preventDefault()
    const response = await fetch(process.env.REACT_APP_API + `/admin/create`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
        body: JSON.stringify(formData)
      })

    const payload = await response.json()
    if (response.status === 201) {
      setStatus("success")
      setErrorMessage("Admin Account created successfully!")
      updateFormData("")
    } else {
      setStatus("error")
      if (payload.message === "validation error") {
        setErrorMessage("Please ensure all fields have been filled")
      } else if (payload.code === "ER_DATA_TOO_LONG") {
        setErrorMessage("Please ensure phone number is in the right format")
      } else {
        setErrorMessage("Admin Account could not be created, please try again")
      }
    }
  }

  return (
    <>
    <Container>
      <Grid container item xs={12} align="center">
        <Box
          sx={{
            marginTop: 8,
            marginBottom: 8,
            marginLeft: 'auto',
            marginRight: 'auto',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <h2 container item xs={12} align="center">Create Your Administrator Account</h2>
          {!status || <Alert severity={status || "info"}>{errorMessage}</Alert>}
          <form align="center" noValidate autoComplete="off" onSubmit={(e) => formSubmit(e)}>
            <TextField sx={{p:1}} fullWidth id="addFullName" name="fullName" label="Full Name" variant="outlined" required onChange={(e) => handleChange(e)} />
            <TextField sx={{p:1}} fullWidth id="addUserName" name="username" label="Username" variant="outlined" required onChange={(e) => handleChange(e)} />
            <TextField sx={{p:1}} fullWidth id="addEmail" name="email" type="email" label="E-Mail" variant="outlined" required onChange={(e) => handleChange(e)} />
            <TextField sx={{p:1}} fullWidth id="addPassword" name="password" helperText="Must be 8 characters or longer" type="password" label="Password" variant="outlined" required onChange={(e) => handleChange(e)} />
            <TextField sx={{p:1}} fullWidth id="addPhoneNumber" name="phoneNumber" type="tel" helperText="Format: 5553331111" label="Phone Number" variant="outlined" required onChange={(e) => handleChange(e)} />
            <br></br>
            <FormLabel>Has admin privileges?</FormLabel>
            <RadioGroup name="admin" onChange={(e) => handleChange(e)}>
              <FormControlLabel value="1" control={<Radio />} label="Yes" />
              <FormControlLabel value="0" control={<Radio />} label="No" />
            </RadioGroup>
            <Button
              style={{ width: '100px' }}
              type="submit"
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Submit
            </Button>
          </form>
        </Box>
      </Grid>
          <br></br>
      </Container>
    </>
  );
};

export default AdminCreate;
