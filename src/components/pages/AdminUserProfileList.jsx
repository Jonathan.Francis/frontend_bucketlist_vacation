import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';
import Container from '@mui/material/Container';

const useStyles = makeStyles({
  table: {
    width: '100%'
  },
});

const AdminUserProfileList = () => {
  const [profiles, setProfiles] = useState([]);
  const history = useHistory();
  const classes = useStyles();

  let token = sessionStorage.getItem("token")

  const profileRoute = (event, profile) => {
    event.preventDefault();
    let path = `/Admin/User/Profile/List/${profile.userid}`;
    history.push(path);
  };


  useEffect(() => {
    async function fetchData() {
      const res = await fetch(process.env.REACT_APP_API + `/profile`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      });
      res
        .json()
        .then((res) => setProfiles(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [token]);

  return (
    <div>
    <Container>
      <h1 align="center">Master List - All User Profiles</h1>
      <TableContainer className={classes.table} component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>User ID:</TableCell>
              <TableCell>Username:</TableCell>
              <TableCell>Email:</TableCell>
              <TableCell>Phone Number: </TableCell>
              <TableCell>Full Name:  </TableCell>
              <TableCell>Admin:</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {profiles.map((profile) => (
              <TableRow key={profile.userid}>
                <TableCell>{profile.userid}</TableCell>
                <TableCell>{profile.username}</TableCell>
                <TableCell>{profile.email}</TableCell>
                <TableCell>{profile.phoneNumber}</TableCell>
                <TableCell>{profile.fullName}</TableCell>
                {/* eslint-disable-next-line */}
                <TableCell>{profile.admin == 1 ? ("Yes") : ("No")}</TableCell>
                <TableCell><Button color="primary" variant="contained" onClick={(e) => profileRoute(e, profile)} component={Paper}>View Profile</Button></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
  </Container>
    </div>
  );
}



export default AdminUserProfileList;