import React, {useState, useEffect} from "react";
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button, Grid, Radio, RadioGroup, FormControlLabel} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

//CSS
const useStyles = makeStyles({
  table: {
    width: '50%',
    marginBottom: '50px',
  },
  grid: {
    marginBottom: '50px',
    width: '50%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  gridHead: {
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 15,
    background: 'aliceblue',
    fontWeight: 'bold',
    width: '30%',
    minWidth: 150,
  },
  gridData: {
    borderStyle: 'solid',
    borderWidth: 1,
    padding: 15,
    width: '70%',
    minWidth: 200,
  },
    margin: {
    marginBottom: 10,
    
  }
  
});


//Page
const AdminView = ({ adminid }) => {

  const classes = useStyles();

  const [savedRes, setSavedRes] = useState("");
  const [admin, setAdmin] = useState({});
  const [updateAdmin, setUpdateAdmin] = useState({});
  const [editMode, setEditMode] = useState(false);
  const [status, setStatus] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

    useEffect(()=>{
      const fetchData = async () => {
        const res = await fetch(process.env.REACT_APP_API + `/admin/get/${adminid}`,
          {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-type' : 'application/json'
          }
        });

        res.json().then((res)=>setAdmin(res))
        .catch((err)=>console.log(err));
      }
        fetchData();
    }, [savedRes, adminid]);

    const toggleEdit = () => {
      setUpdateAdmin(admin)
      setEditMode(true)
    }

    const cancel = () => {
      setStatus(false)
      setErrorMessage("")
      setEditMode(false)
      setUpdateAdmin(admin)
    }

    const handleChange = (e) => {
    let value = e.target.value
    let key = e.target.name
    setStatus(false)
    setUpdateAdmin((prevData) => ({ ...prevData, [key]:value }))
  }

    const formSubmit = async event => {
      event.preventDefault()
      const response = await fetch (process.env.REACT_APP_API + `/admin/update/${adminid}`, 
        {
          method: 'PATCH',
          headers: {
            'Accept': 'application/json',
            'Content-type' : 'application/json'
        },
        body: JSON.stringify(updateAdmin)
      })
  
    // const payload = await response.json()
      if (response.status === 200) {
          setEditMode(false)
          setStatus("success")
          setErrorMessage("Profile updated!")
          setSavedRes(response)
      } else {
          setStatus("error")
          setErrorMessage("Profile could not be updated.")
      }
    }

  return (
    <>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
      <h1 className={classes.margin}>{admin.fullName}</h1>
      {!status || <Alert className={classes.margin} severity={status || "info"}>{errorMessage}</Alert>}
      <form className={classes.margin} noValidate autoComplete="off" onSubmit={(e) => formSubmit(e)}>
        <Grid container spacing={0} className={classes.grid}>
          <Grid item className={classes.gridHead}>Full Name</Grid>
          {editMode ? <TextField fullWidth className={classes.gridData} id="addFullName" value={updateAdmin.fullName} name="fullName" onChange={(e)=> handleChange(e)}/>
          : <Grid item className={classes.gridData}>{admin.fullName}</Grid>}
          <Grid item className={classes.gridHead}>Username</Grid>
          {editMode ? <TextField fullWidth className={classes.gridData} id="addUsername" value={updateAdmin.username} name="username" onChange={(e)=> handleChange(e)}/>
          : <Grid item className={classes.gridData}>{admin.username}</Grid>}
          <Grid item className={classes.gridHead}>Email</Grid>
          {editMode ? <TextField fullWidth className={classes.gridData} id="addEmail" value={updateAdmin.email} name="email" onChange={(e)=> handleChange(e)}/>
          :<Grid item className={classes.gridData}>{admin.email}</Grid>}
          <Grid item className={classes.gridHead}>Phone Number</Grid>
          {editMode ? <TextField fullWidth className={classes.gridData} id="addPhoneNumber" value={updateAdmin.phoneNumber} name="phoneNumber" onChange={(e)=> handleChange(e)}/>
          :<Grid item className={classes.gridData}>{admin.phoneNumber}</Grid>}
          
          <Grid item className={classes.gridHead}>Admin</Grid>
          
          {editMode ?
            <RadioGroup name="isAdmin" onChange={(e)=> handleChange(e)}>
              <FormControlLabel value="1" control={<Radio />} label="Yes" />
              <FormControlLabel value="0" control={<Radio />} label="No" />
            </RadioGroup>
            :<Grid item className={classes.gridData}>{admin.isAdmin ? "Yes" : "No"}</Grid>}

        </Grid>
        <Button className={classes.margin} color="primary" variant="contained" type="submit" >SUBMIT</Button>
        <Button className={classes.margin} variant="contained" onClick={() => toggleEdit()}>EDIT PROFILE</Button>
        <Button className={classes.margin} variant="contained" color="secondary" onClick={() => cancel()}>CANCEL</Button>
        </form>
    </>
  );
};

export default AdminView;
