import * as React from 'react';
import { useState, useEffect } from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button'
import CheckIcon from '@mui/icons-material/Check';
import ShareIcon from '@mui/icons-material/Share';
import DeleteIcon from '@mui/icons-material/Delete';
import Container from '@mui/material/Container'
import AddBucket from './AddBucket'
import EditBucket from './EditBucket';

export default function BucketCheckList() {
  let token = sessionStorage.getItem("token");
  let userid = sessionStorage.getItem("userid").replace(/"/g, "");
  let tripid = null
  const [savedRes, setSavedRes] = useState("")
  const [list, setList] = useState([]);

  const fetchData = async () => {
    const res = await fetch(process.env.REACT_APP_API + `/bucket/${userid}`, {
        method: "GET",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`,
        }
      });
      const payload = await res.json()
      setList(payload)
    }

  // eslint-disable-next-line
  useEffect(() => {fetchData();}, [savedRes, userid, token]);

  const deleteBucketItem = async (event, item) => {
    event.preventDefault();
    await fetch(process.env.REACT_APP_API + `/bucket/${item.bucketid}`, {
      'method': 'DELETE',
      'headers': {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    fetchData()
  }

  const completeBucket = async (event, item) => {
    event.preventDefault();
    await fetch(process.env.REACT_APP_API + `/bucket/${item.bucketid}`, {
      'method': 'PATCH',
      'headers': {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({status: 1})
    })
    fetchData()
  }

  let sortedList = [...list]
  sortedList.sort((a, b) => {
    if (a.status < b.status) {
      return -1
    }
    if (a.status > b.status) {
      return 1
    }
    return 0;
  })

  return (
    <Container>
      <AddBucket tripid = {tripid} setSavedRes = {setSavedRes}/>
      <TableContainer component={Paper} sx={{ marginBottom: 8 }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Title</TableCell>
              <TableCell align="right">Location</TableCell>
              <TableCell align="right">Budget</TableCell>
              <TableCell align="right">Goal</TableCell>
              <TableCell align="right">Target Date</TableCell>
              <TableCell align="right">Status</TableCell>
              <TableCell align="right">Done</TableCell>
              <TableCell align="right">Edit</TableCell>
              <TableCell align="right">Share</TableCell>
              <TableCell align="right">Delete</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sortedList.map((list) => (
              <TableRow
                key={list.name}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {list.name}
                </TableCell>
                <TableCell align="right">{list.location}</TableCell>
                <TableCell align="right">{list.budget}</TableCell>
                <TableCell align="right">{list.category}</TableCell>
                <TableCell align="right">{list.targetDate}</TableCell>
                {/* eslint-disable-next-line */}
                <TableCell align="right">{list.status == 1 ? ("Complete") : ("Incomplete")}</TableCell>
                <TableCell align="right"><Button variant="contained" color="success" onClick={(e) => completeBucket(e, list)}><CheckIcon /></Button></TableCell>
                <TableCell align="right"><EditBucket list={list} /></TableCell>
                <TableCell align="right"><Button variant="contained"><ShareIcon /></Button></TableCell>
                <TableCell align="right"><Button variant="contained" color="error" onClick={(e) => deleteBucketItem(e, list)}><DeleteIcon /></Button></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
}
