import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Card, CardActions, CardMedia, Button, Grid, Container } from '@mui/material';

//Page
const AdminHome = () => {
  const currentdate = new Date();
  var dateformat = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  const datetime = currentdate.toLocaleDateString("en-US", dateformat);
  let userid = sessionStorage.getItem("userid").replace(/"/g, "");

  const [admin, setAdmin] = useState({});


  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(process.env.REACT_APP_API + `/admin/get/${userid}`,
        {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
          }
        });

      res.json().then((res) => setAdmin(res))
        .catch((err) => console.log(err));
    }
    fetchData()
  }, [userid]);

  return (

    
    <Container component="main">
      <Grid>
        <Grid container direction="row">
          <Grid item xs={12}>
            <h1>Welcome {admin.fullName}!</h1>
            <h3>Today is {datetime} </h3>
          </Grid>
        </Grid>
        <Grid container spacing={2} direction="row" >
          <Grid item sm>
            <Card>
              <CardMedia
                component="img"
                alt="User Profiles"
                height="80%"
                width="80%"
                image="/images/icons/users.svg"
                title="User Profiles"
              />
              <CardActions>
                <Link to='/Admin/User/Profile/List'>
                  <Button fullWidth variant="contained" color="primary">All User Profiles</Button>
                </Link>
              </CardActions>
            </Card>
          </Grid>
          <Grid item sm>
            <Card>
              <CardMedia
                component="img"
                alt="Admin Profile"
                height="80%"
                width="80%"
                image="/images/icons/profile.svg"
                title="Admin Profile"
              />
              <CardActions>
                <Link to='/Admin/Profile'>
                  <Button fullWidth variant="contained" color="primary">My Administrator Profile</Button>
                </Link>
              </CardActions>
            </Card>
          </Grid>
            <Grid item sm>
              <Card>
                <CardMedia
                  component="img"
                  alt="Create Admin Profile"
                  height="80%"
                  width="80%"
                  image="/images/icons/create.svg"
                  title="Create Admin Profile"
                />
                <CardActions>
                  <Link to='/Admin/Create'>
                    <Button fullWidth variant="contained" color="primary">Create A Profile</Button>
                  </Link>
                </CardActions>
              </Card>
            </Grid>
            <Grid item sm>
              <Card>
                <CardMedia
                  component="img"
                  alt="View User Submissions"
                  height="80%"
                  width="80%"
                  image="/images/icons/message.svg"
                  title="View User Submissions"
                />
                <CardActions>
                  <Link to='/Admin/Contact'>
                    <Button fullWidth variant="contained" color="primary">View User Submissions</Button>
                  </Link>
                </CardActions>
              </Card>
            </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};

export default AdminHome;
