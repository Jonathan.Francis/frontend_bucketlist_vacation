import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';


const SingleTrip: React.FC = () => {
  return (
    <div>
      <Grid container style={{ minHeight: '100vh' }}>
        <Grid item xs={12} sm={6}>
          
        </Grid>
        <Grid container item xs={12} sm={6} alignItems="center" direction="column" style={{ padding: 10 }}>
          <div />
          <div style={{ display: "flex", flexDirection: "column", maxWidth: 400, minWidth: 400 }}>
          <Typography variant="h4">TRIP</Typography>
            <Grid container>
              <TextField label="Bucket Name" margin="normal" fullWidth />
              <TextField label="Location" margin="dense" fullWidth />
              <TextField label="Budget" margin="dense" fullWidth />
              <TextField label="Category" margin="dense" fullWidth />
              <TextField label="Target Date" variant="outlined" margin="dense" fullWidth />
              <TextField label="Participants" variant="outlined" margin="dense" fullWidth />
              <div style={{ display: "flex", flexDirection: "column" }} />
              <TextField name="upload-photo" type="file" fullWidth/>
              <Button color="primary" variant="text"> Save </Button>
              <Button color="warning" variant="text"> Cancel </Button>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default SingleTrip;
