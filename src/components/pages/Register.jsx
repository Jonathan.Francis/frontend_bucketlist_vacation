import { useState } from "react";
import { Button, Box, TextField, Typography, Container, CssBaseline, Alert } from "@mui/material";
import PersonOutlinedIcon from '@mui/icons-material/PersonOutlined';
import Avatar from '@mui/material/Avatar';


const Register = () => {
  const [registerForm, setRegisterForm] = useState({ fullName: "", email: "", phoneNumber: "", username: "", password: "" });
  const [failure, setFailure] = useState(true)
  const [success, setSuccess] = useState(false)
  const [message, setMessage] = useState("")
  const handleChange = e => setRegisterForm({ ...registerForm, [e.target.name]: e.target.value });

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/register`, {
      method: "POST",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(registerForm),
    });
    const payload = await response.json();
    if (response.status >= 400 && payload.message === 'e-mail address already exists') {
      console.error();
      setFailure(false)
      setMessage("E-mail already exists. Please use a different one.")
    } else if (response.status >= 400) {
      console.error();
      setFailure(false)
      setMessage("Some of your fields are incorrect. Unable to register your account.")
    } else {
      setSuccess(true)
      setMessage("Congratulations, you have created a new account for your Bucket List Adventures!")
      setRegisterForm({ fullName: "", email: "", phoneNumber: "", username: "", password: "" })
    }
  };


  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}

      >
        <Avatar>
          <PersonOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Registration
        </Typography>
        <Typography variant="subtitle1">
          Please fill in the details below to register on this site.
        </Typography>
        {!failure && (<Alert severity="error">{message}</Alert>)}
        {success && (<Alert severity="success">{message}</Alert>)}
        <Box
          component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}
        >
          <TextField
            fullWidth
            required
            id="addRegisterFullName"
            name="fullName"
            label="Full Name"
            variant="standard"
            type="text"
            autoFocus
            value={registerForm.fullName}
            onChange={handleChange}
          />
          <TextField
            fullWidth
            required
            id="addRegisterEmail"
            name="email"
            label="E-mail"
            variant="standard"
            type="email"
            value={registerForm.email}
            onChange={handleChange}
          />
          <TextField
            fullWidth
            required
            id="addRegisterPhoneNumber"
            name="phoneNumber"
            label="Phone Number"
            variant="standard"
            type="tel"
            value={registerForm.phoneNumber}
            onChange={handleChange}
          />
          <TextField
            fullWidth
            required
            id="addRegisterUsername"
            name="username"
            label="Username"
            variant="standard"
            type="text"
            value={registerForm.username}
            onChange={handleChange}
          />
          <TextField
            fullWidth
            required
            id="addRegisterPassword"
            name="password"
            label="Password"
            variant="standard"
            type="password"
            value={registerForm.password}
            onChange={handleChange}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Submit
          </Button>
        </Box>
      </Box>
    </Container>
  );
}





export default Register;