import { Button, Container, TextField, Box, Typography } from "@mui/material";
import { useHistory } from "react-router-dom";

const ViewContactSubmission = (props) => {
  const history = useHistory();
  let entry = props.location.state;

  const backButton = () => {
    history.push('/Admin/Contact')
  }

  return (
    <Container component="main">
      <Box sx={{
        marginTop: 4,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: "center"
      }}>

        <h2>View Contact Form Entry</h2>
        <Typography sx={{ width: 800, p: 1 }}>
          <div>
            <TextField sx={{p:1}} fullWidth type="text" label="Contact ID" name="contactid" defaultValue={entry.contactid} disabled={true} />
          </div>
          <div>
            <TextField sx={{p:1}} fullWidth type="text" label="User ID" name="userid" defaultValue={entry.userid} disabled={true} />
          </div>
          <div>
            <TextField sx={{p:1}} fullWidth type="text" label="Email" name="email" defaultValue={entry.email} disabled={true} />
          </div>
          <div>
            <TextField sx={{p:1}} fullWidth type="text" label="Message" name="message" multiline rows={10} defaultValue={entry.message} disabled={true} />
          </div>
        </Typography>
        <Button variant="contained" color="info" onClick={backButton}>Back</Button>
      </Box>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
    </Container>
  );
};

export default ViewContactSubmission;
