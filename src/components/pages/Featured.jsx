import React, { useState, useEffect } from "react";
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import CardMedia from '@mui/material/CardMedia';
import { Link } from "react-router-dom";

export default function Featured() {

    const [list, setList] = useState([]);
  
    let userid = sessionStorage.getItem("userid").replace(/"/g, "");
    useEffect(() => {
      async function fetchData() {
        const res = await fetch(process.env.REACT_APP_API + `/featured`, {
            method: "GET"
        });
        res
        .json()
        .then((res) => setList(res))
        .catch((err) => console.log(err));
      }
      fetchData();
    }, [userid]);
  
  return (
    <Container component="main">
    <CssBaseline />
    <main>
    <br></br>
    <br></br>
    <br></br>
    <Typography variant="h2" align="center">
        Featured

      </Typography>
        {list.map((list) => (
          <Container>
            <Typography variant="h2">
              {list.name}
            </Typography>
            <Typography variant="h4">
              {list.description}
            </Typography>
            <Link to={{
              pathname: `/EditTrip/${list.tripid}`,
              state: { tripinfo: list}}}>
              VIEW
            </Link>
            <CardMedia
              component="img"
              sx={{
                height: "500px"
              }}
              image="https://source.unsplash.com/random"
              alt="random"
             />
             <br></br>
             <br></br>
             <br></br>
             <br></br>
             <br></br>
             <br></br>
          </Container>
        ))}
    </main>
  </Container>
  );
}