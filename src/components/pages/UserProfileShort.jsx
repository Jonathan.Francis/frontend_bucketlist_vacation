import * as React from 'react';
import { useState, useEffect } from 'react'
import { Container, ImageList, ImageListItem, Typography, Button, TextField, Box } from '@mui/material';
import parseJwt from '../helpers/authHelper'
import { useHistory } from 'react-router-dom';

export default function UserProfileShort() {
  const token = sessionStorage.getItem("token");
  const email = parseJwt(token).email;
  const [user, setUser] = useState([]);
  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(process.env.REACT_APP_API + `/auth/user/${email}`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        }
      });
      res
        .json()
        .then((res) => setUser(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  },[email,token]);

  const editProfile = (event, user) => {
    event.preventDefault();
    let path = `/UserProfile`
    history.push(path, user)
  }


  return (
    <Container component="main" maxWidth="sm">
      <Box sx={{ marginTop: 4, display: 'flex', m: 1 }}>
        <ImageList>
          <ImageListItem>
            <img src={user.image} alt="This is your profile"></img>
          </ImageListItem>
          <Box>
            <Typography>
              <TextField sx={{ m: 1 }} value={user.fullName} variant="filled" disabled/>
              <TextField sx={{ m: 1 }} value={user.email} variant="filled" disabled/>
              <TextField sx={{ m: 1 }} value={user.interests} variant="filled" disabled/>
            </Typography>
          </Box>
        </ImageList>
      </Box>
      <Box sx={{ display: 'flex', justifyContent: "left", m: 1 }}>
        <Button size="small" variant="contained" onClick={(e) => editProfile(e, user)}>View/Edit My Profile</Button>
      </Box>
    </Container>
  )
}
