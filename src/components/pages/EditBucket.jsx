import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { TextField, Typography, InputLabel, MenuItem, Select } from '@mui/material/';
import Modal from '@mui/material/Modal';
import { useState } from 'react';
import EditIcon from '@mui/icons-material/Edit';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '70%',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function EditBucket(props) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  let token = sessionStorage.getItem("token");
  const [category, setCategory] = useState(props.list.category)
  const [status, setStatus] = useState(props.list.status)


  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
  };

  const handleStatusChange = (event) => {
    setStatus(event.target.value);
  };

  const [bucket, setBucket] = useState(props.list);
  bucket.category = category;
  bucket.status = status;

  const handleChange = (event) => {
    setBucket({ ...bucket, [event.target.name]: event.target.value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(process.env.REACT_APP_API + `/bucket/${bucket.bucketid}`, {
      method: "PUT",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(bucket),
    });
    if (response.status >= 400) {
      console.error();
    } else {
      setBucket({ name: "", location: "", image: "", budget: "", category: "", targetDate: "", participants: "", status: "" })
      setCategory("")
      setStatus("")
      handleClose()
    }
    window.location.reload()
  };

  return (
    <div>
      <Button variant="contained" color="success" onClick={handleOpen}><EditIcon /></Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <form onSubmit={handleSubmit}>
          <Box sx={style} >
            <Typography id="modal-modal-title" variant="h6" component="h2" textAlign="center">
              Bucketlist Item Details
            </Typography>
            <TextField
              fullWidth
              required
              id="name"
              name="name"
              label="Name"
              variant="standard"
              type="text"
              value={bucket.name}
              onChange={handleChange}
              autoFocus
            />
            <TextField
              fullWidth
              required
              id="location"
              name="location"
              label="Location"
              variant="standard"
              type="text"
              value={bucket.location}
              onChange={handleChange}
            />
            <TextField
              fullWidth
              required
              id="budget"
              name="budget"
              label="Budget"
              variant="standard"
              type="text"
              value={bucket.budget}
              onChange={handleChange}
            />
            <InputLabel id="demo-simple-select-label">Category</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={category}
              label="Category"
              onChange={handleCategoryChange}
              sx={{ width: 1 / 4 }}
            >
              <MenuItem value={'Sports'}>Sports</MenuItem>
              <MenuItem value={'Entertainment'}>Entertainment</MenuItem>
              <MenuItem value={'Animal Watching'}>Animal Watching</MenuItem>
              <MenuItem value={'Food'}>Food</MenuItem>
              <MenuItem value={'Travel'}>Travel</MenuItem>
              <MenuItem value={'Friends and Family'}>Friends and Family</MenuItem>
              <MenuItem value={'Self Improvement'}>Self Improvement</MenuItem>
              <MenuItem value={'Influential'}>Influential</MenuItem>
            </Select>
            <p>Target Date</p>
            <TextField
              fullWidth
              required
              id="targetDate"
              name="targetDate"
              variant="standard"
              type="date"
              value={bucket.targetDate}
              onChange={handleChange}
            />
            <TextField
              fullWidth
              required
              id="participants"
              name="participants"
              label="Participants"
              variant="standard"
              type="text"
              value={bucket.participants}
              onChange={handleChange}
            />
            <InputLabel id="demo-simple-select-label">Status</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={status}
              label="Status"
              onChange={handleStatusChange}
              sx={{ width: 1 / 4 }}
            >
              <MenuItem value={1}>Complete</MenuItem>
              <MenuItem value={0}>Incomplete</MenuItem>
            </Select>
            <Button
              type="submit"
              variant="contained"
              sx={{ mt: 3, mb: 2, justifyContent: "center" }}
              fullWidth
            >
              Submit
            </Button>
          </Box>
        </form>
      </Modal>
    </div>
  );
}
