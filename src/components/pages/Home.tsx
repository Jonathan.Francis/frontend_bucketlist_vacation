import * as React from 'react';
import Container from '@mui/material/Container';
import BucketList from './BucketList';
import UserProfileShort from './UserProfileShort';
import BucketCheckList from './BucketCheckList';

export default function Home() {

    return (
        <React.Fragment>
            <Container component="main" maxWidth="lg">
                <UserProfileShort />    
                <BucketList />
                <BucketCheckList />
            </Container>
        </React.Fragment>
    );
}
