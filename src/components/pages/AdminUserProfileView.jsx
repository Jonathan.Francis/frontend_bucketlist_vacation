import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { TextField, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, FormControlLabel, Radio, RadioGroup, FormLabel } from '@mui/material';
import { makeStyles } from '@mui/styles';
import AdminAddBucket from "./AdminAddBucket";
import EditBucket from './EditBucket';
import CheckIcon from '@mui/icons-material/Check';
import ShareIcon from '@mui/icons-material/Share';
import DeleteIcon from '@mui/icons-material/Delete';

const useStyles = makeStyles({
  textfield: {
    marginBottom: 10
  },
  table: {
    width: '100%',
    marginBottom: 40,
  },
  dateHeader: {
    width: 205,
  },
  margin: {
    margin: "0 10px 20px 0",
    width: '130%'
  },
  subheading: {
    margin: "50px 5px 20px 0",
    textAlign: "left"
  },
});



const AdminUserProfileView = (props) => {
  let id = props.match.params.id; 
  const [savedRes, setSavedRes] = useState("");
  const [profiles, setProfiles] = useState([]);
  const [bucket, setBucket] = useState([]);
  const [form, setForm] = useState({ display: "none" });
  const [profile, setProfile] = useState({ username: "", email: "", password: "", phoneNumber: "", fullName: "", admin: "" });
  const history = useHistory();
  const classes = useStyles();
  let token = sessionStorage.getItem("token")
  let tripid = null

  async function fetchData() {
    const res = await fetch(process.env.REACT_APP_API + `/profile/${id}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    });
    res.json().then((res) => setProfiles(res));

    const res2 = await fetch(process.env.REACT_APP_API + `/bucket/${id}`, {
      method: "GET",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`,
      }
    });
    const payload = await res2.json()
    setBucket(payload)
  }

  //eslint-disable-next-line
  useEffect(() => {fetchData();}, [savedRes, id, token]);


  const handleDelete = (event) => {
    event.preventDefault();

    fetch(process.env.REACT_APP_API + `/profile/${id}`, {
      method: "delete",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
    }).then((response) => response.json());
    history.push("/Admin/User/Profile/List");
  };


  const handleEdit = (event, profile) => {
    event.preventDefault();
    setForm({ display: "block" });
    setProfile(profile);
  };


  const handleChange = e => setProfile({ ...profile, [e.target.name]: e.target.value });

  const handleSubmit = (event, profile) => {
    event.preventDefault();
    fetch(process.env.REACT_APP_API + `/profile/${id}`, {
      method: "PATCH",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },

      //make sure to serialize your JSON body
      body: JSON.stringify(profile),
    }).then((response) => setSavedRes(response));
    setForm({ display: "none" })
    fetchData()
  };

  const deleteBucketItem = async (event, item) => {
    event.preventDefault();
    await fetch(process.env.REACT_APP_API + `/bucket/${item.bucketid}`, {
      'method': 'DELETE',
      'headers': {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
    fetchData()
  }


  return (
    <div>
      <h1 align="center" >Admin - View User Profile</h1>
      <TableContainer className={classes.table} component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>User ID:</TableCell>
              <TableCell>Username:</TableCell>
              <TableCell>Email:</TableCell>
              <TableCell>Phone Number: </TableCell>
              <TableCell>Location: </TableCell>
              <TableCell>Interests: </TableCell>
              <TableCell>Full Name:  </TableCell>
              <TableCell>Admin:</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {profiles.map((profile) => (
              <TableRow key={profile.userid}>
                <TableCell>{profile.userid}</TableCell>
                <TableCell>{profile.username}</TableCell>
                <TableCell>{profile.email}</TableCell>
                <TableCell>{profile.phoneNumber}</TableCell>
                <TableCell>{profile.location}</TableCell>
                <TableCell>{profile.interests}</TableCell>
                <TableCell>{profile.fullName}</TableCell>
                {/* eslint-disable-next-line */}
                <TableCell>{profile.admin == 1 ? ("Yes") : ("No")}</TableCell>
                <TableCell><Button variant="contained" color="primary" onClick={(e) => { handleEdit(e, profile); }} component={Paper}>Edit</Button></TableCell>
                <TableCell><Button variant="contained" color="error" onClick={(e) => { handleDelete(e); }} component={Paper}>Delete</Button></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <AdminAddBucket tripid = {tripid} setSavedRes = {setSavedRes} id = {id}/>
      <TableContainer component={Paper} sx={{ marginBottom: 8 }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Title</TableCell>
              <TableCell align="right">Location</TableCell>
              <TableCell align="right">Budget</TableCell>
              <TableCell align="right">Goal</TableCell>
              <TableCell align="right">Due Date</TableCell>
              <TableCell align="right">Done</TableCell>
              <TableCell align="right">Edit</TableCell>
              <TableCell align="right">Share</TableCell>
              <TableCell align="right">Delete</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {bucket.map((list) => (
              <TableRow
                key={list.name}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {list.name}
                </TableCell>
                <TableCell align="right">{list.location}</TableCell>
                <TableCell align="right">{list.budget}</TableCell>
                <TableCell align="right">{list.category}</TableCell>
                <TableCell align="right">{list.targetDate}</TableCell>
                <TableCell align="right"><Button variant="contained" color="success"><CheckIcon /></Button></TableCell>
                <TableCell align="right"><EditBucket list={list} /></TableCell>
                <TableCell align="right"><Button variant="contained"><ShareIcon /></Button></TableCell>
                <TableCell align="right"><Button variant="contained" color="error" onClick={(e) => deleteBucketItem(e, list)}><DeleteIcon /></Button></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>


      {/* This is the form that pops up when you press the Edit Me! button */}
      <form style={form}>
        <div>
          <TextField
            fullWidth
            className={classes.textfield}
            variant="outlined"
            label="Username:"
            type="text"
            name="username"
            value={profile.username}
            onChange={handleChange}
          />
        </div>
        <div>
          <TextField
            fullWidth
            className={classes.textfield}
            variant="outlined"
            label="Email:"
            type="e-mail"
            name="email"
            value={profile.email}
            onChange={handleChange}
          />
        </div>
        <div>
          <TextField
            fullWidth
            className={classes.textfield}
            variant="outlined"
            label="Password:"
            type="password"
            name="password"
            value={profile.password}
            onChange={handleChange}
          />
        </div>
        <div>
          <TextField
            fullWidth
            className={classes.textfield}
            label="Phone Number:"
            variant="outlined"
            type="tel"
            name="phoneNumber"
            value={profile.phoneNumber}
            onChange={handleChange}
          />
        </div>
        <div>
          <TextField
            fullWidth
            className={classes.textfield}
            label="Full Name:"
            variant="outlined"
            type="text"
            name="fullName"
            value={profile.fullName}
            onChange={handleChange}
          />
        </div>
        <div>
        <TextField
            fullWidth
            className={classes.textfield}
            label="Location:"
            variant="outlined"
            type="text"
            name="location"
            value={profile.location}
            onChange={handleChange}
          />
        </div>
        <div>
        <TextField
            fullWidth
            className={classes.textfield}
            label="Interests:"
            variant="outlined"
            type="text"
            name="interests"
            value={profile.interests}
            onChange={handleChange}
          />
        </div>
        <div>
          <FormLabel>Has admin privileges?</FormLabel>
          <RadioGroup name="admin" value={profile.admin} onChange={(e) => handleChange(e)}>
            <FormControlLabel value="1" control={<Radio />} label="Yes" />
            <FormControlLabel value="0" control={<Radio />} label="No" />
          </RadioGroup>
        </div>

        <Button className={classes.textfield} fullWidth variant="contained" color="primary" onClick={(e) => handleSubmit(e, profile)} component={Paper}>Submit</Button>
        <Button className={classes.textfield} fullWidth onClick={() => { setForm({ display: "none" }) }}>Cancel</Button>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
      </form>
    </div>
  );
}


export default AdminUserProfileView;

