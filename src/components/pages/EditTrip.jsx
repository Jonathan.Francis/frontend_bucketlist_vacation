import React, { useState } from "react";
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Switch from '@mui/material/Switch';
import { Container, CssBaseline } from '@mui/material'
import { useHistory } from "react-router-dom";
import AddTripBucketList from './AddTripBucketList';

let EditTrip = (props) => {

	const history = useHistory();
	const tripRoute = () => {
			let path = `/Trips`
			history.push(path);
	};

  const [trip, setTrip] = useState(props.location.state.tripinfo);
  const [featured, setFeatured] = useState(props.location.state.tripinfo.featured);
  trip.featured = featured;

  const handleChange = (event) => {
    setTrip({ ...trip, [event.target.name]: event.target.value });
  };

  const handleFeaturedChange = (event) => {
    setFeatured(event.target.checked);
  };

	const handleSubmit = async (event) => {
    event.preventDefault();
		const response = await fetch(process.env.REACT_APP_API + `/trip/${trip.tripid}`, {
			method: "PUT",
			headers: {
				"Accept": "application/json",
				"Content-Type": "application/json",
			},
			body: JSON.stringify(trip),
		});
		const payload = await response.json();
		if (response.status >= 400 && payload.message === 'Error: Did not create trip') {
			console.error();
		} else {
			alert("Trip Updated")
			tripRoute()
		}
	};

  return (
    <Container component="main">
        <CssBaseline />
		<Typography
					variant="h5"
					align="center"
					gutterBottom
					sx={{
					marginTop: 8
				}}>

            Edit Trip
        </Typography>
					<form onSubmit={(e) => handleSubmit(e)}>
						<Stack
							style={{
								maxWidth: "500px",
								margin: "auto"
						}}>
						<TextField
							type="text"
							id="name"
							name="name" 
							label="Trip Name" 
							variant="standard"
							value={trip.name}
							onChange={handleChange}>
						</TextField>
						<TextField
							type="text"
							id="description"
							name="description" 
							label="Trip Description" 
							variant="standard"
							value={trip.description}  
							multiline
							rows={6} 
							onChange={handleChange}>
						</TextField>
            <div>
              Featured
              <Switch
                label="featured"
                name="featured"
                checked={trip.featured}
                onChange={handleFeaturedChange}/>
            </div>
						</Stack>
						<AddTripBucketList tripid = {trip.tripid}/>
						<Button
							variant="contained" 
							type="submit">
							Done
         		</Button>
					</form>
					<br></br>
					<br></br>
					<br></br>
    </Container>
  );
}

export default EditTrip