import { gql, useQuery } from "@apollo/client";
import { User } from "../types";

export const ME = gql`
  query me {
    email
    id
    password
    username
  }
`;

export const USERS = gql`
  query users {
    users {
      email
      id
      password
      username
    }
  }
`;

export function useUsers() {
  const { data, loading, refetch, error } = useQuery(USERS);
  // const user: User = data?.me;
  console.log(data);
  // return { user, loading, refetch, error };
}

export function useCurrentUser() {
  const { data, loading, refetch, error } = useQuery(ME);
  const user: User = data?.me;
  return { user, loading, refetch, error };
}
